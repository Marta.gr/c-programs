#include <stdio.h>
 #include <stdlib.h>

 int main(){
 
  unsigned primo[] = {2, 3, 5, 7, 11, 13, 17, 19, 23};

  printf( "PRIMO:\n"
  "======\n"
  "Localización (%p)\n"
  " Elementos: %lu[%u..%u]\n"
  " Tamaño: %lu bytes.\n\n",
     primo,
   sizeof(primo) / (sizeof(int),
     primo[0], primo[8],
     sizeof(primo));
 return EXIT_SUCCESS;
 }
