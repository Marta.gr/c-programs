#include <stdio.h>
#include <stdlib.h>

#define N 15

int main(){
double resultado;
int elemento[N];
/*Condiciones de contorno*/
  elemento[1]=elemento[0]=1;

/* Calculo*/
  for (int i =2; i<N; i++)
      elemento[i] = elemento[i-1] + elemento[i-2];

 /*Salida de datos*/
  for (int i=0; i<N; i++)
      printf("%i    ", elemento[i]); 
      printf("\n");

  for(int i=1; i<N; i++)
printf("%.2lf ", (double) elemento[i]/elemento[i-1]);
printf("\n");
    return EXIT_SUCCESS;

}
