#include <stdio.h>
#include <stdlib.h>

int main(){

 unsigned primo[]   = {2, 3, 5, 7, 11, 13, 17, 19, 23};
 unsigned elementos = (unsigned) sizeof(primo)/ sizeof(int);
 unsigned *peeping  = primo;
 char *tom = (char *) primo;
 unsigned **police = &peeping;

 printf ("PRIMO:\n"
         "======\n"
         "Localización (%p)\n"
         " Elementos: %u [%u..%u]\n"
         " Tamaño: %lu bytes.\n\n",
          primo,
          elementos,
          primo[0], primo[elementos-1],
           sizeof(primo));

//anotacion de matrices
 printf("0: %u\n", peeping[0]);
 printf("1: %u\n", peeping[1]);

//anotacion de punteros
 printf("0: %u\n", *peeping);
 printf("1: %u\n", *(peeping+1));
 printf("Tamaño: %lu bytes.\n" ,sizeof(peeping) );
 printf( "\n" );

 /* Memory Dump - Volcado de memoria */

 for (int i=0; i<sizeof(primo); i++)
 printf("%02X", *(tom+i));
 printf( "\n\n" );

 printf("Police contine %p\n", police );
 printf("Peeping contine %p\n", *police );
 printf("Primo[0]+1 contine %lu\n", *(*police+1));
		 

//police contiene a peeping
//peeping contiene a la direccion de lo que contiene peeping que es la direcion de la celda
	return EXIT_SUCCESS;

}
