#include <stdio.h>
#include <stdlib.h>

#define E 0.000000000001

double fc (int base, int prof){
	if (prof == 1)
		return base;
	return base +1./ fc (base, prof - 1);
}

int main(int argc, char *argv[]){

	int base, i;
	double res0 = 0, res1=-100;


	printf("Fracción continua del numero: ");
	scanf(" %i", &base);

	for( int (res1-res0)>E && i<20; i++){
		res0 = res1;
		res1 = fc (base, i);
	}


	printf("fracción continua (%i, %i) = %.2lf \n", base,i, res1);

	return EXIT_SUCCESS;

}
