#include <stdio.h>
#include <stdlib.h>

const char *program_name;
void print_usage (int exit_code){
	FILE *f = stdout;
	if(exit_code != 0)
		f=stderr;
	fprintf(f,
		       "  - Checks for normality of the argument. - \n\n"
		       "Usage:	%s <number>\n\n"
		       "Number: Positive integer. \n"
		       "\n", 
		       
		       program_name);
	exit (exit_code);

}
bool es_primo(int pprimo) {
	bool primo = true;

		for(int d=pprimo/2; primo==true && d>1; d--)
			if(pprimo % d == 0)
				primo = false;

	return primo;
}

int main(int argc, char * aurgv[]){

	program_name = argv[0];
	if(argc < 2)
		print_usage (1);

	int n = atoi (argv[1]);

	printf(" %s es primo el %i. \n", es_primo(n) ? "Si" : "No", n); 
	// ? es como un if, si es verdadero es si, si es falso es No



	return EXIT_SUCCESS;

}
