#include <stdio.h>
#include <stdlib.h>

#define CUADRADO 1
#define CIRCULO 2
#define RECTANGULO 3
#define TRIANGULO 4

int main(){

int  menu;
double base, altura, lado, radio, resultado;

    printf("Dime de que figura quieres saber el area.Elige el numero de las opciones\n");
    printf("Estas son las opciones: \n");
    printf("1. Cuadrado \n");
    printf("2. Circulo \n");
    printf("3. Rectangulo \n");
    printf("4. Triangulo \n");

    scanf(" %i", &menu);

    switch(menu){

        case 1:

    printf(" Has elegido la opción 1, es el cuadrado. \n");
    printf("Dime las medidas en cm del lado del cuadrado: ");
    scanf("%lf", &lado);
    resultado=lado*lado;
    printf("El área del cuadrado con el lado indicado es %.2lf\n", resultado);

        case 2:

    printf(" Has elegido la opción 2, es el circulo. \n");
    printf("Dime las medidas en cm del radio del circulo: ");
    scanf("%lf", &radio);
    resultado=radio*3.14;
    printf("El área es %.2lf\n", resultado);

        case 3:

    printf(" Has elegido la opción 3, es el rectangulo. \n");
    printf("Dime la medida en cm de la base del rectangulo : ");
    scanf("%lf", &base);
    printf("Dime la medida en cm de la altura del rectangulo: ");
    scanf("%lf", &altura);
    resultado=base*altura;
    printf("El área es %.2lf\n", resultado);

        case 4:

    printf(" Has elegido la opción 4, es el triangulo. \n");
    printf("Dime la medida en cm de la base del triangulo : ");
    scanf("%lf", &base);
    printf("Dime la medida en cm de la altura del triangulo: ");
    scanf("%lf", &altura);
    resultado=base*altura/2;
    printf("El área es %.2lf\n", resultado);



    }

	return EXIT_SUCCESS;

}
