#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#define angulo 60.0
#define PI 3.14
#define P PI/180

int main(){
	double  resultado;
	int angulo;
	printf("Dime de que angulo quieres sacar el coseno ");
	scanf(" %i", &angulo);

	resultado = cos(angulo*P);

	printf("El coseno del angulo %i es %.2lf\n", angulo, resultado);

	/*
	   for (double grados=0; grados<K*360; grados+=K*.5)
	   printf("%.2lf%s\n",(double) grados, unidad);

*/
	return EXIT_SUCCESS;
}
