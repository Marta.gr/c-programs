#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#define N 10

unsigned dv(unsigned celda [N][N], int f, int c){
    if (f<0 || c<0 || c>f)
        return (unsigned) 0;
    return celda[f][c];

}

int main(){

    unsigned tart[N][N];
    bzero(tart, sizeof(tart));

    tart[0][0] = 1;
    for(int fila=1; fila<N; fila++)
        for(int col=0; col<=fila; col++)
            tart[fila][col] = dv(tart, fila-1, col-1) + dv(tart, fila-1, col);

    for(int fila=0; fila<N; fila++){
        for(int col=0; col<=fila; col++)
            printf(" %i", tart[fila][col]);
        printf("\n");

    }

    printf("\n");

	return EXIT_SUCCESS;

}
