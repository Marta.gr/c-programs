#include <stdio.h>
#include <stdlib.h>

#define N 9
#define INPUT "fibonacci.dat"


int main(int argc, char *argv[]){


	int datos [N];
	FILE *pf;

	if( !(pf = fopen(INPUT, "rb"))){
		fprintf(stderr, "Arrrrgh. Muero. \n");
	return EXIT_FAILURE;
	}


	fread (datos, sizeof (int), N, pf);
	fclose(pf);

	for( int i=0; i<N; i++)
		printf("%i  ", datos[i]);

	printf("\n");


	return EXIT_SUCCESS;

}
