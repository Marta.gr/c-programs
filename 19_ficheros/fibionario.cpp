#include <stdio.h>
#include <stdlib.h>

#define N 9
#define DUMP "fibonacci.dat"

int rellenar (int fi[N], int i){
	if(i == 1){
		fi[0] = 1;
		return fi[1] = 1;
	}
return fi [i] = rellenar (fi, i-1) + fi[i-2];
}

int main(int argc, const char **argv){

	FILE *pf;
	int fibo[N];

	rellenar (fibo, N-1);
	
	if (! (pf = fopen (DUMP, "wb"))){   //no se ha podido abrir el fichero
	
		fprintf(stderr, "Ajin.\n");
		return EXIT_FAILURE;
	}
fwrite(fibo, sizeof(int), n, pf); // fibo es la direccion de memoria de la primera celda

fclose(pf);
	return EXIT_SUCCESS;

}
