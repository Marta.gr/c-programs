#include <stdio.h>
#include <stdlib.h>


int main(){

	int n;
	char *puntcadena;
	printf("Ingrese el numero de caracteres de la cadena:");
	scanf("%d",&n);
	getchar();
	puntcadena=(char*)malloc(n*sizeof(char));

	printf("\nSe ha reservado espacio de memoria para %d caracteres", n);

	printf("\nIngrese la cadena:");
	scanf("%s", &puntcadena);

	if(sizeof(puntcadena)>n){
	printf("Debe de meter una cadena de caracteres con la longitud indicada\n");
	}else{
	printf("El valor almacenado en la variable de la cadena es %s \n", puntcadena);
	}
	return EXIT_SUCCESS;
}

