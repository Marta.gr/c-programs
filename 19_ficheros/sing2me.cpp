
#include <stdio.h>
#include <stdlib.h>

#define CANCION "cancion.txt"

int main(int argc, char *argv[]){

	const char * cancion;
	char c;
	FILE *pf;
	

 	if(argc<2)
		return EXIT_FAILURE;

	cancion = argv[1];
	if( !(pf = fopen (cancion, "r")) )
		return EXIT_FAILURE;
	
	while ( (c = getc (pf)) != EOF)
		printf("%c", c);

	fclose (pf);

	return EXIT_SUCCESS;

}
