#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define x 0
#define y 0
#define DIM 2

int main(){

    double vector[2][DIM],
           modulo[2],
           producto,
           angulo;


  for(int i=0; i<2; i++){

  printf("Vector %i: ", i + 1);
  scanf(" %lf, %lf", &vector [i] [x], &vector[i][y]);
                        }
                  

  for (int i=0; i<2; i++)
    modulo[i] = sqrt(
        pow(vector[i][x], 2) +
        pow(vector[i][y], 2)
                    );

  for(int c=0; c<DIM; c++)
    producto += vector[0][c] * vector[1][c];

  angulo= acos (producto / modulo[0] / modulo[1]);
  angulo *= 180/ M_PI;

  printf("Angulo = %.2lf º\n", angulo);

	return EXIT_SUCCESS;

}
