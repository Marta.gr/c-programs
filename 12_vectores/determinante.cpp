#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Función punto de entrada*/
int main(){
      double r, a00, a10, a01, a20, a11, a21, a02, a12, a22;
      double a[3][3];

   printf("Introduce el valor de a00: ");
   scanf("%lf", &a00);

   printf("Introduce el valor de a10: ");
   scanf("%lf", &a10);

   printf("Introduce el valor de a20: ");
   scanf("%lf", &a20);

   printf("Introduce el valor de a01: ");
   scanf("%lf", &a01);

   printf("Introduce el valor de a11: ");
   scanf("%lf", &a11);


   printf("Introduce el valor de a21: ");


   printf("Introduce el valor de a02: ");   
   scanf("%lf", &a02);   
   printf("Introduce el valor de a12: ");   
   scanf("%lf", &a12);   
   printf("Introduce el valor de a22: ");   
   scanf("%lf", &a22);   printf("\n");   
   printf("Matriz\n");   
   printf("| %.lf    %.lf    %.lf |\n", a00, a01, a02);   
   printf("| %.lf    %.lf    %.lf |\n", a10, a11, a12);   
   printf("| %.lf    %.lf    %.lf |\n", a20, a21, a22);   
   printf("\n");   
   
   r = a00 * a11 * a22 +       
       a10 * a21 * a02 +      
       a20 * a01 * a12 - 
       a02 * a11 * a20 -      
       a21 * a12 * a00 -  
       a01 * a10 * a22;
   printf("El determinante de la matriz es: %.lf \n", r);
   return EXIT_SUCCESS;


}

