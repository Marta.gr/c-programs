#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

void pon_titulo(int numero){ /*Parámetro formal */
	/*Título */
		 char titulo[MAX];
	         sprintf(titulo, "toilet -fpagga --metal Tabla del %i", numero);
	         system(titulo);

	}

/* Función punto de entrada */
int main(){

	/*Declaración de variables */
	 int tabla, op1=1;
	
	/*Menu*/ 
	 printf("¿Qúe tabla quieres?\n");
	 printf("Tabla: ");
	 scanf(" %i", &tabla);

	pon_titulo(tabla); /* Llamada con parámetro 5 */

	/* resultados */

	 printf("%ix%i=%i\n", op1, tabla, op1*tabla);
	 op1++;
	 printf("%ix%i=%i\n",op1, tabla, op1*tabla);


	return EXIT_SUCCESS;

}
